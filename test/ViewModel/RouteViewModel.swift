//
//  RouteViewModel.swift
//  test
//
//  Created by Juan Emmanuel Cepeda Antillon on 27/07/20.
//  Copyright © 2020 Juan Emmanuel Cepeda Antillon. All rights reserved.
//

import Foundation
import RealmSwift

protocol RouteViewModelDelegate {
    func getRoutesFromRealm(routeArr: [RouteObject])
}

struct RouteViewModel {
    
    var route : RouteObject
    var delegate: RouteViewModelDelegate?
    var routesArr = [RouteObject]()
    
    func fetchRoutes() {
        var routesArr = [RouteObject]()
        do {
            let routes = try Realm().objects(RouteObject.self)
            
            for route in routes {
                routesArr.append(route)
            }
            delegate?.getRoutesFromRealm(routeArr: routesArr)
        }
        catch{
            //TODO: Create an alert to handler an error while try to fectch routes
        }
    }
    
    
    
    
}
