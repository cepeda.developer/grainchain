//
//  RouteTableViewCell.swift
//  test
//
//  Created by Juan Emmanuel Cepeda Antillon on 26/07/20.
//  Copyright © 2020 Juan Emmanuel Cepeda Antillon. All rights reserved.
//

import UIKit

class RouteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var routeNameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
           viewCell.layer.borderWidth = 2
           viewCell.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
           viewCell.clipsToBounds = true
           viewCell.layer.cornerRadius = 20
           self.selectionStyle = .none
    }
    
    func configure(route: RouteObject) {
        routeNameLabel.text = route.identifier
        distanceLabel.text = "\(String(format: "%.3f", route.distance))km"
        timeLabel.text = route.time
    }
    
}
