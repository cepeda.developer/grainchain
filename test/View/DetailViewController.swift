//
//  DetailViewController.swift
//  test
//
//  Created by Juan Emmanuel Cepeda Antillon on 27/07/20.
//  Copyright © 2020 Juan Emmanuel Cepeda Antillon. All rights reserved.
//

import UIKit
import GoogleMaps
import RealmSwift

class DetailViewController: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    var route = RouteObject()
    var path = GMSMutablePath()
    var index = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        drawRoute()
    }
    
    func setupUI(){
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.layer.borderWidth = 2
        mapView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        mapView.clipsToBounds = true
        mapView.layer.cornerRadius = 20
        timeLabel.text = route.time
        distanceLabel.text = "\(String(format: "%.3f", route.distance))km"
        
        let camera = GMSCameraPosition(target: route.path.coordinate(at: 0), zoom: 10 )
        self.mapView.animate(to: camera)
    }
    
    func drawRoute(){
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = .red
        polyline.strokeWidth = 100.0
        polyline.map = mapView
    }
    
    func share(){
        
        
        
    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        do {
            let realm = try Realm()
            
            let objects = realm.objects(RouteObject.self)
            
            try! realm.write {
                realm.delete(objects)
            }
        } catch let error as NSError {
            // handle error
            print("error - \(error.localizedDescription)")
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
