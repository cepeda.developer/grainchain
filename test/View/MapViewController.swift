//
//  MapViewController.swift
//  test
//
//  Created by Juan Emmanuel Cepeda Antillon on 21/07/20.
//  Copyright © 2020 Juan Emmanuel Cepeda Antillon. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import RealmSwift

class MapViewController: UIViewController, GMSMapViewDelegate, RouteViewModelDelegate {

    //MARK: UIOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var routeNameTextField: UITextField!
    @IBOutlet weak var recButton: UIButton!
    
    //MARK: Properties
    let locationManager = CLLocationManager()
    var firstLocation : CLLocationCoordinate2D?
    var lastLocation : CLLocationCoordinate2D?
    var isFirstLocation = true
    var path = GMSMutablePath()
    var isRecording = false
    var distance : Double? = 0.0
    var timeString : String = ""
    let cellIdentifier = "cell"
    let routeCell = "RouteTableViewCell"
    var routesArr = [RouteObject]()
    var time = 0
    var timer = Timer()
    
    var routeViewModel : RouteViewModel?{
        didSet{
            tableView.reloadData()
        }
    }
    
    //MARK: Implementation
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getLocation()
        setpUI()
        routeViewModel = RouteViewModel(route: RouteObject())
        routeViewModel?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        routeViewModel?.fetchRoutes()
    }
    
    func setpUI(){
        navigationController?.isNavigationBarHidden = true
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.layer.borderWidth = 2
        mapView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        mapView.clipsToBounds = true
        mapView.layer.cornerRadius = 20
        tableView.rowHeight = 100.0
        tableView.register(UINib(nibName: routeCell, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        
        guard let routeName = routeNameTextField.text else { return }
        if (isRecording && routeName == "") { return }
        
        if !isRecording {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(routeTime), userInfo: nil, repeats: true)
            routeNameTextField.isHidden = false
        }
        isRecording = !isRecording
    }
}

//MARK: Location Manager
extension MapViewController: CLLocationManagerDelegate {
    
    func getLocation() {
        let status = CLLocationManager.authorizationStatus()
        
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            return
        case .denied, .restricted:
            let alert = UIAlertController(title: "Location Services disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            
            present(alert, animated: true, completion: nil)
            return
        case .authorizedAlways, .authorizedWhenInUse:
            break
            
        @unknown default: break
            // TODO: To create an alert to check this case
        }
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    //MARK: Location Manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations.last {
            fetchRoute(location: currentLocation.coordinate)
            let camera = GMSCameraPosition(target: currentLocation.coordinate, zoom: 18 )
            self.mapView.animate(to: camera)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
//MARK: Route
    
    func fetchRoute(location: CLLocationCoordinate2D) {
        if !isRecording {
            if firstLocation != nil {
                distance = firstLocation?.distance(from: location)
                guard  let distanceStr = distance  else { return }
                guard let routeName = routeNameTextField.text else { return }
                if routeName == "" {return}
                let route = Route(identifier: routeName, distance: distanceStr/1000, time: timeString, path: path)
                
                let container = try! Container()
                try! container.write { transaction in
                    transaction.add(route, update: true)
                }
                path.removeAllCoordinates()
                addMark(location: location, final: true)
                time = 0
                timer.invalidate()
                firstLocation = nil
                recButton.setTitle("REC", for: .normal)
                routeNameTextField.isHidden = true
                routeNameTextField.text = ""
                routeViewModel?.fetchRoutes()
            }
            return
        }
        path.add(CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
        if firstLocation == nil {
            firstLocation = location
            recButton.setTitle("Save Route", for: .normal)
            addMark(location: firstLocation!, final: false)
        }
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = .red
        polyline.strokeWidth = 2.0
        polyline.map = mapView
    }
    
    func addMark(location: CLLocationCoordinate2D, final: Bool) {
        let marker = GMSMarker()
        if final { marker.icon = #imageLiteral(resourceName: "flag") }
        marker.position = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        marker.map = mapView
    }
    
    @objc func routeTime(){
        time += 1
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .full
        let formattedString = formatter.string(from: TimeInterval(time))!
        timeString = formattedString
        print(timeString)
    }
    
    
    func getRoutesFromRealm(routeArr: [RouteObject]) {
        routesArr.removeAll()
        routesArr = routeArr
        tableView.reloadData()
    }
}

//MARK: Tableview Implementation

extension MapViewController: UITableViewDelegate, UITableViewDataSource{
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return routesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RouteTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! RouteTableViewCell
        cell.configure(route: routesArr[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController = DetailViewController()
        let route = routesArr[indexPath.row]
        detailViewController.index = indexPath.row
        detailViewController.route = route
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}
