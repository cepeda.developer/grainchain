//
//  LocationCoordinate2D+Distance.swift
//  test
//
//  Created by Juan Emmanuel Cepeda Antillon on 25/07/20.
//  Copyright © 2020 Juan Emmanuel Cepeda Antillon. All rights reserved.
//

import Foundation
import GoogleMaps

extension CLLocationCoordinate2D {
    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
        let destination=CLLocation(latitude:from.latitude,longitude:from.longitude)
        return CLLocation(latitude: latitude, longitude: longitude).distance(from: destination)
    }
}
