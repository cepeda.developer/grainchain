//
//  Route.swift
//  test
//
//  Created by Juan Emmanuel Cepeda Antillon on 24/07/20.
//  Copyright © 2020 Juan Emmanuel Cepeda Antillon. All rights reserved.
//

import Foundation
import RealmSwift
import GoogleMaps

struct Route {
    
  public let identifier: String
  public let distance : Double
  public let time : String
  public let path : GMSMutablePath
}

final class RouteObject: Object {
    @objc dynamic var identifier = ""
    @objc dynamic var time = ""
    @objc dynamic var distance = 0.0
    
    dynamic var path = GMSMutablePath()
    override static func primaryKey() -> String? {
        return "identifier"
    }
}

public protocol Persistable {
    associatedtype ManagedObject: RealmSwift.Object
    init(managedObject: ManagedObject)
    func managedObject() -> ManagedObject
}

extension Route: Persistable {
    public init(managedObject: RouteObject) {
        identifier = managedObject.identifier
        path = managedObject.path
        distance = managedObject.distance
        time = managedObject.time
    }
    
    public func managedObject() -> RouteObject {
        let route = RouteObject()
        route.identifier = identifier
        route.path = path
        route.distance = distance
        route.time = time
        return route
    }
}

